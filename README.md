# React Snippets

React Snippets for Visual Studio Code

## Snippets List

- `_rrd`: Render DOM
- `_rcc`: Component Class
- `_rsf`: Stateless Function
